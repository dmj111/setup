#!/bin/bash -x

REPO_DIR=$HOME/repos

## Oh-my-zsh setup

from_github() {
    name=$1
    if [[ -d $REPO_DIR/$name ]]; then
	echo "$name exists"
    else
	git clone git@github.com:dmj111/${name}.git $REPO_DIR/$name
    fi
}

from_bitbucket_hg_obs() {
    name=$1
    if [[ -d $REPO_DIR/$name ]]; then
	echo "$name exists"
    else
	echo "getting $name from bitbucket"
	hg clone ssh://hg@bitbucket.org/dmj111/$name $REPO_DIR/$name
    fi
}


from_bitbucket_git() {
    name=$1
    if [[ -d $REPO_DIR/$name ]]; then
	echo "$name exists"
    else
	echo "getting $name from bitbucket"
	git clone ssh://hg@bitbucket.org/dmj111/${name}.git $REPO_DIR/$name
    fi
}



from_github oh-my-zsh
from_github oh-my-zsh-custom


oh_my_zsh=$REPO_DIR/oh-my-zsh

if [[ -d $oh_my_zsh/custom ]]; then
    mv $oh_my_zsh/custom $oh_my_zsh/custom.old
fi

ln -sf $REPO_DIR/oh-my-zsh-custom $oh_my_zsh/custom
ln -sf $oh_my_zsh/custom/config/zshrc $HOME/.zshrc
ln -sf $oh_my_zsh/custom/config/zshenv $HOME/.zshenv
ln -sf $oh_my_zsh $HOME/.oh-my-zsh




# Emacs setup
from_bitbucket_git emacs
ln -sf $REPO_DIR/emacs $HOME/.emacs.d

from_bitbucket_git config
$REPO_DIR/config/setup.sh
ln -s $REPO_DIR/config ~/config

from_bitbucket_git setup
