#!/bin/zsh -x


echo "Run as sudo to install stuff that I use a lot"

packages=(
    autoconf
    autoconf-archive
    autoconf-doc
    autoconf2.13
    automake
    automake1.9-doc
    binutils-doc
    binutils-gold
    bison
    bison-doc
    build-essential
    debian-keyring
    emacs24
    emacs24-el
    flex
    g++-4.7-multilib
    g++-multilib
    gcc-4.7-doc
    gcc-4.7-locales
    gcc-4.7-multilib
    gcc-doc
    gcc-multilib
    gdb
    gdb-doc
    gdbserver
    gettext
    gfortran
    gfortran-doc
    gfortran-multilib
    git
    glibc-doc
    gnu-standards
    gnupg-agent
    keychain
    lib64mudflap0
    libgcc1-dbg
    libgomp1-dbg
    libitm1-dbg
    libmudflap0-4.7-dev
    libmudflap0-dbg
    libquadmath0-dbg
    libstdc++6-4.7-dbg
    libstdc++6-4.7-doc
    libtool
    libtool-doc
    libx32mudflap0
    make
    make-doc
    mercurial
    python3
    python3-pip
    python-pip
    zsh-doc
    )



# for pack in $packages; do
#     apt-get --yes install $pack
# done

apt-get install $packages

# doc-base?

# gcj
